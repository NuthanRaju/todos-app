//
//  UpdateViewController.swift
//  Todo
//
//  Created by Nuthan raju on 27/06/19.
//  Copyright © 2019 Google. All rights reserved.
//

import UIKit
protocol DataSender {
    func senditem(item:Item)
    func update()
}

class UpdateViewController: UIViewController {
    
    @IBOutlet weak var textField: UITextField!
    
    
    @IBOutlet weak var textView: UITextView!
    
    
    var delegate: DataSender? = nil
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
    }
    
    
    
    @IBAction func saveButtonTapped(_ sender: Any) {
        if textField.text == "" {
            showAlert(title: "TextField is empty", message: "Enter Text")
            return
        }
        if textView.text == "" {
            showAlert(title: "TextView is Empty", message: "Enter Text")
            return
        }
        let item = Item(title: textField.text ?? "", message: textView.text ?? "")
        self.delegate?.senditem(item: item)
        self.navigationController?.popViewController(animated: true)
        
    }
    func showAlert(title:String,message:String)  {
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        let ok = UIAlertAction(title: "OK", style: .default) { (action) in
            print("OK Pressed")
        }
        alert.addAction(ok)
        self.present(alert,animated:true) {
            print("")
        }
    }
}
