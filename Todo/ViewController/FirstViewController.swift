//
//  ViewController.swift
//  Todo
//
//  Created by Nuthan raju on 27/06/19.
//  Copyright © 2019 Google. All rights reserved.
//

import UIKit

class FirstViewController: UIViewController {
    var itemList  = [Item]()
    
    
    @IBOutlet weak var tableView: UITableView!
    var isCompleted: Bool = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        self.tableView.dataSource = self
        self.tableView.delegate = self
        
        if let itemsData = UserDefaults.standard.value(forKey: "Items") as? Data  {
            if let listItem = try? NSKeyedUnarchiver.unarchiveTopLevelObjectWithData(itemsData) as? [Item] {
                if let unwrappedItems = listItem {
                    self.itemList = unwrappedItems
                }
                
            }
        }
    }
    
    
    @IBAction func addButtonTapped(_ sender: Any) {
        
        let desVc = self.storyboard?.instantiateViewController(withIdentifier: "UpdateViewController") as? UpdateViewController
        desVc!.delegate = self
        self.navigationController?.pushViewController(desVc!, animated: true)
        
    }
    
    func updateItemListInDefaults() {
        if let data = try?NSKeyedArchiver.archivedData(withRootObject: self.itemList, requiringSecureCoding: false) {
            UserDefaults.standard.set(data, forKey: "Items")
            UserDefaults.standard.synchronize()
            self.tableView.reloadData()
        }
    }
}
extension FirstViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return itemList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if let cell = tableView.dequeueReusableCell(withIdentifier: "cell")  as? TableViewCell {
            let item = itemList[indexPath.row]
            cell.titleLabel.text = item.title
            if item.isCompleted {
                cell.backgroundColor = .green
            }
            return cell
            
        }
        return UITableViewCell()
    }
    
    
}
extension FirstViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        print(indexPath.row)
        print(indexPath.section)
        let itemSelected = itemList[indexPath.row]
        if let createVC = self.storyboard?.instantiateViewController(withIdentifier: "CreateViewController") as? CreateViewController {
            createVC.delegate = self
            createVC.selectedItem = itemSelected
            self.present(createVC, animated: true, completion: nil)
            
        }
        
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 90
    }
    func tableView(_ tableView: UITableView, leadingSwipeActionsConfigurationForRowAt indexPath: IndexPath) -> UISwipeActionsConfiguration? {
        let delete = deletedAction(at:indexPath)
        let config = UISwipeActionsConfiguration(actions: [delete])
        config.performsFirstActionWithFullSwipe = false
        return config
    }
    func deletedAction(at indexPath: IndexPath) -> UIContextualAction {
        let action = UIContextualAction(style: .destructive, title: "Delete") {[weak self] (action, view, completion) in
            
            self?.itemList.remove(at: indexPath.row)
            self?.tableView.deleteRows(at: [indexPath], with: .automatic)
            self?.updateItemListInDefaults()
            completion(true)
        }
        
        return action
    }
    func tableView(_ tableView: UITableView, trailingSwipeActionsConfigurationForRowAt indexPath: IndexPath) -> UISwipeActionsConfiguration? {
        let complete = completedAction(at:indexPath)
        let config = UISwipeActionsConfiguration(actions: [complete])
        config.performsFirstActionWithFullSwipe = false
        return config
        
    }
    func completedAction(at indexPath: IndexPath) -> UIContextualAction {
        let action = UIContextualAction(style: .normal, title: "Status") {[weak self] (action, view, completion) in
            
            if let item = self?.itemList[indexPath.row] {
                item.isCompleted = !item.isCompleted
                self?.updateItemListInDefaults()
                
                completion(true)
                
            }
        }
        return action
    }
    
}
extension FirstViewController: DataSender {
    func senditem(item: Item) {
        self.itemList.append(item)
        self.updateItemListInDefaults()
        self.tableView.reloadData()
    }
    
    func update(){
        self.updateItemListInDefaults()
        self.tableView.reloadData()
    }
}
