//
//  TODO.swift
//  Todo
//
//  Created by Nuthan raju on 28/06/19.
//  Copyright © 2019 Google. All rights reserved.
//

import Foundation
class Item: NSObject, NSCoding
{
    var title : String
    var message : String
    var isCompleted: Bool = false
    init(title: String,message: String, isCompleted: Bool = false) {
        self.title = title
        self.message = message
        self.isCompleted = isCompleted
    }
    func encode(with aCoder: NSCoder) {
        aCoder.encode(title, forKey: "Title")
        aCoder.encode(message, forKey: "Message")
        aCoder.encode(isCompleted, forKey: "IsCompleted")
    }
    
    required convenience init?(coder aDecoder: NSCoder) {
        let title = aDecoder.decodeObject(forKey: "Title") as? String ?? ""
        let message = aDecoder.decodeObject(forKey: "Message") as? String ?? ""
        let isCompleted = aDecoder.decodeBool(forKey: "IsCompleted")
        
        self.init(title: title, message: message, isCompleted: isCompleted)
    }
}
