//
//  CreateViewController.swift
//  Todo
//
//  Created by Nuthan raju on 27/06/19.
//  Copyright © 2019 Google. All rights reserved.
//

import UIKit

class CreateViewController: UIViewController {
    
    @IBOutlet weak var label: UILabel!
    
    @IBOutlet weak var textView: UITextView!
    
    @IBOutlet weak var textField: UITextField!
    
    @IBOutlet weak var textView2: UITextView!
    
    @IBOutlet weak var createButton: UIButton!
    
    @IBOutlet weak var cancelButton2
    : UIButton!
    var selectedItem: Item?
    var delegate:DataSender? = nil
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        
        // Do any additional setup after loading the view.
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        if let item = selectedItem {
            label.text = item.title
            textView.text = item.message
        }
        textField.isHidden = true
        textView2.isHidden = true
        createButton.isHidden = true
        cancelButton2.isHidden = true
    }
    
    @IBAction func cancelButtonTapped(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func updateButtonTapped(_ sender: Any) {
        textField.isHidden = false
        textView2.isHidden = false
        createButton.isHidden = false
        cancelButton2.isHidden = false
        textField.text = selectedItem?.title
        textView2.text = selectedItem?.message
        
    }
    
    @IBAction func createButtonTapped(_ sender: Any) {
        selectedItem?.title = textField.text ?? ""
        selectedItem?.message = textView2.text ?? ""
        self.delegate?.update()
        self.dismiss(animated: true, completion: nil)
        
        
    }
    
    @IBAction func cancelButtonTapped2(_ sender: Any) {
        textField.text = ""
        textView2.text = ""
        textField.isHidden = true
        textView2.isHidden = true
        createButton.isHidden = true
        cancelButton2.isHidden = true
        
    }
}
